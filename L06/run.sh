#!/usr/bin/env bash

ansible-playbook create-dir5.yml \
        -i inventories/dev/hosts \
        -c local \
        "$@"
